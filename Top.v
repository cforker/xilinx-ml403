`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Chris Forker
// 
// Create Date:    20:40:53 05/23/2016 
// Design Name: 
// Module Name:    Top 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module Top(
	input 			Sysclk,
	input 			ResetN,
	
	input  [4:0]	PushbuttonsI, // N, S, E, W, C
	
	output [8:0] 	LED, // 3, 2, 1, 0, N, S, E, W, C
	//output [18:0] 	VGA,
	
	output [3:0]  	LCD_Data,
	output [2:0]   LCD_Ctrl // EN, RS, RW
	//output [15:0] 	DebugQO
	
	);
	
//	wire clk_150;
	wire clk_100;
//	wire clk_200;
	
//	wire [7:0] 	addrAQ, addrBQ;
//	wire [7:0]	dInAQ, dInBQ;
//	wire [7:0] 	dOutAQ, dOutBQ;
//	wire 			weaQ, webQ;
	
	reg [4:0] PushbuttonsQ;
	
//	assign addrAQ = 8'h0;
//	assign addrBQ = 8'h0;
//	assign dInAQ = 8'h0;
//	assign dInBQ = 8'h0;
//	assign weaQ = 1'b0;
//	assign webQ = 1'b0;
	
	// wire [8:0] 	LED;
   //	wire [18:0] VGA;
	// wire [3:0] 	LCD_Data;
	// wire [2:0] 	LCD_Ctrl;
	// wire [15:0] DebugQO;
	
	assign LED[4:0] = PushbuttonsQ;
	assign LED[8:5] = 4'hF;
	assign LCD_Data = 4'h0;
	assign LCD_Ctrl = 3'h0;

	always @ (posedge clk_100)
	if (~ResetN) begin
		PushbuttonsQ <= 5'h0;
	end else begin
		PushbuttonsQ <= PushbuttonsI;
	end

	ClockGen ClockGen (
		 .CLKIN_IN			(Sysclk), 
		 .RST_IN				(~ResetN), 
		 .CLKFX_OUT			(), 
		 .CLKIN_IBUFG_OUT	(clk_100), 
		 .CLK0_OUT			(), 
		 .CLK2X_OUT			(), 
		 .LOCKED_OUT		()
		 );

//	GameRAM GameRAM (
//	  .clka(clk_100), // input clka
//	  .wea(weaQ), 		// input [0 : 0] wea
//	  .addra(addrAQ), // input [7 : 0] addra
//	  .dina(dInAQ), 	// input [7 : 0] dina
//	  .douta(dOutAQ), // output [7 : 0] douta
//	  .clkb(clk_100), // input clkb
//	  .web(webQ), 		// input [0 : 0] web
//	  .addrb(addrBQ), // input [7 : 0] addrb
//	  .dinb(dInBQ), 	// input [7 : 0] dinb
//	  .doutb(dOutBQ) 	// output [7 : 0] doutb
//	);


endmodule
