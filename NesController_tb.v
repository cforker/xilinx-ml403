`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   21:51:51 05/27/2016
// Design Name:   NesController
// Module Name:   C:/Users/cforker/Work/ML403A/NesController_tb.v
// Project Name:  ML403A
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: NesController
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module NesController_tb;

	// Inputs
	reg clk;
	reg ResetN;
	reg ReadNesQI;
	reg NesDataI;

	// Outputs
	wire [7:0] NesButtonsQO;
	wire ButtonsValidQO;
	wire NesLatchQO;
	wire NesClockQO;

	// Instantiate the Unit Under Test (UUT)
  // Change the NES clock period to 60ns to make simulation easier
	NesController #(60) uut (
		.clk(clk), 
		.ResetN(ResetN), 
		.ReadNesQI(ReadNesQI), 
		.NesButtonsQO(NesButtonsQO), 
		.ButtonsValidQO(ButtonsValidQO), 
		.NesLatchQO(NesLatchQO), 
		.NesClockQO(NesClockQO), 
		.NesDataI(NesDataI)
	);
  
  always #5 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
		ResetN = 0;
		ReadNesQI = 0;
		NesDataI = 0;

		// Wait 100 ns for global reset to finish
		#100
        
		// Add stimulus here
    ResetN = 1;
    
    #100
    ReadNesQI = 1;
    #20
    ReadNesQI = 0;
    
    #100
    NesDataI = 1;
    #210
    NesDataI = 0;
    #140
    NesDataI = 1;
    #140
    NesDataI = 0;
    #140
    NesDataI = 1;
    #140
    NesDataI = 0;
    #140
    NesDataI = 1;
    #140
    NesDataI = 0;

	end
      
endmodule

