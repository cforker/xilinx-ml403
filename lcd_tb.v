`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   23:37:00 05/27/2016
// Design Name:   lcd
// Module Name:   C:/Users/cforker/Work/ML403A/lcd_tb.v
// Project Name:  ML403A
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: lcd
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module lcd_tb;

	// Inputs
	reg clk;
  reg reset_n;

	// Outputs
	wire lcd_rs;
	wire sf_d8;
	wire sf_d9;
	wire sf_d10;
	wire sf_d11;
	wire lcd_rw;
	wire lcd_e;
	wire sf_ce0;

	// Instantiate the Unit Under Test (UUT)
	lcd uut (
		.clk(clk), 
    .reset_n(reset_n),
		.lcd_rs(lcd_rs), 
		.sf_d8(sf_d8), 
		.sf_d9(sf_d9), 
		.sf_d10(sf_d10), 
		.sf_d11(sf_d11), 
		.lcd_rw(lcd_rw), 
		.lcd_e(lcd_e), 
		.sf_ce0(sf_ce0)
	);

  always #5 clk = ~clk;

	initial begin
		// Initialize Inputs
		clk = 0;
    reset_n = 0;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here
    reset_n = 1;

	end
      
endmodule

