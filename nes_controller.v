`timescale 1ns / 1ps

module NesController(
                     input            clk, // 100MHz
                     input            ResetN,
                     input            ReadNesQI,

                     output reg [7:0] NesButtonsQO, // A, B, select, start, up, down, left, right
                     output reg       ButtonsValidQO, // one hot pulse when new buttons are available

                     output reg       NesLatchQO,
                     output reg       NesClockQO,
                     input            NesDataI            
                     );

   parameter NES_PERIOD_ns = 6000;
   parameter CLK_PERIOD_ns = 10;
   parameter SLOW_CLK_COUNT = NES_PERIOD_ns / CLK_PERIOD_ns;
   
   reg                                slow_clk, NesDataQ, ReadNesQ;
   reg [15:0]                         SlowCountQ;                         
   reg [4:0]                          CountQ;
   reg [7:0]                          NesButtonsQ;
   
   always @ (posedge clk)
     if (~ResetN) begin
        NesButtonsQO <= 8'h0;
        NesButtonsQ <= 8'h0;
        ButtonsValidQO <= 1'b0;
        NesLatchQO <= 1'b0;
        NesClockQO <= 1'b0;
        ReadNesQ   <= 1'b0;
        NesDataQ <= 1'b0;
        CountQ <= 5'h13;
     end else begin
        if (slow_clk) begin
           if (CountQ == 5'h13)
             if (ReadNesQ)
               CountQ <= 5'h0;
             else
               CountQ <= CountQ;           
           else
             CountQ <= CountQ + 1;

           // Latch high on Count values 0 and 1 only.
           NesLatchQO <= ~|CountQ[4:1];         

           // Clock has 8 pulses starting at Count value 3, on odd pulses, not on 0x13
           NesClockQO <= CountQ[0] & |CountQ[4:1] & ~(CountQ == 5'h13);

           // Read a data bit and pulse the clock at the same time.
           if (CountQ[0] & |CountQ[4:1] & ~(CountQ == 5'h13)) begin
              NesButtonsQ <= {NesButtonsQ[6:0], ~NesDataQ};
              NesClockQO <= CountQ[0] & |CountQ[4:1] & ~(CountQ == 5'h13);
           end                 

           // Done reading data on clock 0x12
           if (CountQ == 5'h12) begin
              ButtonsValidQO <= 1'b1;
              NesButtonsQO <= NesButtonsQ;
           end
           
        end // if (slow_clk)
        else begin 
           ButtonsValidQO <= 1'b0;
        end

        // Latch the ReadNesQI signal until it's needed on the next slow_clk
        ReadNesQ <= ReadNesQI | (ReadNesQ & ~slow_clk);
        
        NesDataQ <= NesDataI;
     end
     
   // Slow clock logic
   always @ (posedge clk)
     if (~ResetN) begin
        slow_clk <= 1'b0;
        SlowCountQ <= 16'h0;
     end else begin
        if (SlowCountQ == SLOW_CLK_COUNT) begin
           slow_clk <= 1'b1;
           SlowCountQ <= 16'h0;
        end else begin
           slow_clk <= 1'b0;
           SlowCountQ <= SlowCountQ + 1;
        end
     end   
   
endmodule
   
